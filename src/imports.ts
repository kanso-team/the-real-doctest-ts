// TODO: Import everything from the-real-doctest npm library

export const DOCTEST_RESULT_LOG_ID = "DOCTEST >> ";

export type TestResult = { path: `${string}:${number}` } & (
  | { type: 'error', message: string }
  | { type: 'success' }
)

export const fileExtensions = [
  ".js",
  ".ts",
  ".cts",
  ".mts",
  ".cjs",
  ".mjs",
  ".tsx",
  ".jsx"
]