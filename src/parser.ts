import * as vscode from 'vscode';


export const parseTSDoc = (text: string, events: {
	onTest(range: vscode.Range, lineNo: number): void;
}) => {
	const lines = text.split('\n');

	for (let lineNo = 0; lineNo < lines.length; lineNo++) {
		const line = lines[lineNo];
		if (line.match(/^\s*\*\s*@example/)) {
			const range = new vscode.Range(new vscode.Position(lineNo, 0), new vscode.Position(lineNo, line.length));
			events.onTest(range, lineNo);
			continue;
		}
	}
};
