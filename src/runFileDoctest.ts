import * as fs from "fs"
import { execSync } from "child_process"
import { dirname } from "path"
import { DOCTEST_RESULT_LOG_ID, TestResult } from "./imports";



export async function runFileDoctest({ path, lines }: {
  path: string, lines: number[]
}): Promise<TestResult[]> { 
  // Find project path with package.json in it
  let projectPath = path;
  let runCommand: null | string = null
  const iterationLimit = 36 // To avoid infinite looping, just in case
  for (let i = 0; i < iterationLimit; ++i) {
    projectPath = dirname(projectPath)
    if (i >= iterationLimit || projectPath === "/") {
      throw new Error(
        "Couldn't find a package.json with the-real-doctest installed in parent directories.\n" +
        "Please look at the setup section:\n"+
        "https://www.npmjs.com/package/the-real-doctest"
      )
    }
    const packageJsonExists = fs.existsSync(`${projectPath}/package.json`)
    if (!packageJsonExists) {
      continue;
    }

    const packageJson = JSON.parse(fs.readFileSync(`${projectPath}/package.json`).toString())
    // Check that the-real-doctest is installed
    if (!("the-real-doctest" in { ...packageJson?.devDependencies, ...packageJson?.dependencies })) {
      continue;
    }

    runCommand = packageJson?.["the-real-doctest"]?.["runCommand"]
    if (!runCommand) throw new Error(
      "No run command found in package.json. Please run: \n" +
      " >> npx the-real-doctest install"
    )
    break;
  }

  // Run the module
  const cmd = `cd ${projectPath} && ${runCommand} ${path}`
  console.log(`Running ${cmd}`)
  const processResult = execSync(cmd).toString()
  const resultLine = processResult.split("\n")
    .filter(line => line.startsWith(DOCTEST_RESULT_LOG_ID))
    .map(line => JSON.parse(line.substring(DOCTEST_RESULT_LOG_ID.length)) as TestResult)
    .filter(r => lines.some(line => r.path.endsWith(`${path}:${line}`)))

  if (!resultLine) throw new Error("No output from the-real-doctest")
  return resultLine
}