import { TextDecoder } from 'util';
import * as vscode from 'vscode';
import { parseTSDoc } from './parser';
import { runFileDoctest } from './runFileDoctest';
import { execSync } from 'child_process';
import { TestResult } from './imports';

const textDecoder = new TextDecoder('utf-8');

export type DocTestData = TestFile | TestCase;

export const testData = new WeakMap<vscode.TestItem, DocTestData>();

let generationCounter = 0;

export const getContentFromFilesystem = async (uri: vscode.Uri) => {
	try {
		const rawContent = await vscode.workspace.fs.readFile(uri);
		return textDecoder.decode(rawContent);
	} catch (e) {
		console.warn(`Error providing tests for ${uri.fsPath}`, e);
		return '';
	}
};

export class TestFile {
	public didResolve = false;

	public async updateFromDisk(controller: vscode.TestController, item: vscode.TestItem) {
		try {
			const content = await getContentFromFilesystem(item.uri!);
			item.error = undefined;
			this.updateFromContents(controller, content, item);
		} catch (e) {
			item.error = (e as Error).stack;
		}
	}

	/**
	 * Parses the tests from the input text, and updates the tests contained
	 * by this file to be those from the text,
	 */
	public updateFromContents(controller: vscode.TestController, content: string, item: vscode.TestItem) {
		const ancestors = [{ item, children: [] as vscode.TestItem[] }];
		const thisGeneration = generationCounter++;
		this.didResolve = true;

		const ascend = (depth: number) => {
			while (ancestors.length > depth) {
				const finished = ancestors.pop()!;
				finished.item.children.replace(finished.children);
			}
		};

		parseTSDoc(content, {
			onTest: (range, lineNo) => {
				const parent = ancestors[ancestors.length - 1];
				const data = new TestCase(item.uri?.path ?? "??", lineNo, content, thisGeneration);
				const id = `${item.uri}/${data.getLabel()}`;

				const tcase = controller.createTestItem(id, data.getLabel(), item.uri);
				testData.set(tcase, data);
				tcase.range = range;
				parent.children.push(tcase);
			},
		});

		ascend(0); // finish and assign children for all remaining items
	}
}


export class TestCase {
	constructor(
		private readonly file: string,
		private readonly line: number,
		private readonly content: string,
		public generation: number
	) { }

	getLabel() {
		return `Doctest line ${this.line}`;
	}

	async run(item: vscode.TestItem, options: vscode.TestRun): Promise<void> {
		const start = Date.now();
		const result = await this.evaluate();
		const duration = Date.now() - start;
		

		if (result.type == 'success') {
			options.passed(item, duration);
		} else {
			// TODO: Parse output to use diff
			// const message = vscode.TestMessage.diff(`Expected ${item.label}`, String(expected), String(actual));
			const message = new vscode.TestMessage(`Error at ${result.path} => ${result.message}`);
			message.location = new vscode.Location(item.uri!, item.range!);
			options.failed(item, message, duration);
		}
	}

	private async evaluate(): Promise<TestResult> {
		return await new Promise(async (resolve) => {
			try {
				const resultList = await runFileDoctest({
					path: this.file,
					lines: [this.line + 1],
				})
				const result = resultList.at(0)
				if (!result) throw new Error("No result!")
				resolve(result)
			} catch (e: any) {
				let msg = "Error while running the test: " + e?.message
				if (e?.message?.includes("Unable to compile TypeScript")) {
					msg = e.message.substring(e.message.indexOf("Unable to compile TypeScript"))
				}
				resolve({
					path: `${this.file}:${this.line}`,
					type: "error",
					message: msg,
				})
			}
		});
	}
}
