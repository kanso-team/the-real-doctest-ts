<!--
 Copyright (c) 2023 Kanso

 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# The Real Doctest TS

Run TSDoc @example tags like unit tests.
All equality expression statements are checked like a unit test

### Example Usage

```ts
/**
 * @param n
 * @returns the sum of the n first integers
 * @example
 * const n = 5
 * const expected = 1 + 2 + 3 + 4 + 5
 * const actual = nsum(n)
 * actual == expected
 * @example nsum(3) == 1 + 2 + 3
 * @example nsum(8) == 42 // This should fail
 */
function nsum(n: number): number {
  return n * (n + 1) / 2
}
```

### Setup

Run `npm i -D the-real-doctest tsx` in your typescript project root
